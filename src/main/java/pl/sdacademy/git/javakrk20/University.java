package pl.sdacademy.git.javakrk20;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/*
This class should be encapsulated and immutable
Additionally it requires to have toString() method,
similarly as equals(Object) and hashcode().
 */

public final class University {
     String name;
     List<Register> registers = new ArrayList<>();

     University(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "University{" +
                "name='" + name + '\'' +
                ", registers=" + registers +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof University)) return false;
        University that = (University) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(registers, that.registers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, registers);
    }

    /*
        This class should be completely deeply immutable except remove() method.
        And maybe some almost obligatory methods should be in there?
        Register should also have keeper that is also a person.
        And this class shouldn't be inner, should it?
        */
    public static class Register {
        private String name;
        private List<Person> students = new ArrayList<>();

        Register(String name) {
            this.name = name;
        }

        void addStudent(Person student) {
            students.add(student);
        }

        List<Person> getStudents() {
            return students;
        }

        String getName() {
            return name;
        }

        public void setStudents(List<Person> students) {
            this.students = students;
        }
    }
}
