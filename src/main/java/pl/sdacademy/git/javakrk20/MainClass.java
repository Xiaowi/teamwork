package pl.sdacademy.git.javakrk20;

import java.util.Arrays;

public class MainClass {

    /*
    This method is doing to many various things.
    It's not it's responsibility (due to SOLID).
     */
    public static void main(String[] args) {
        University.Register usos = new University.Register("USOS");
        Person p1 = new Student("Tomek", "Bartosz", "12345678910", true,true);
        Person p2 = new Student("Bartek", "Tomasz", "10987654321", true,true);
        usos.addStudent(p1);
        usos.addStudent(p2);

        University uj = new University("UJ");
        uj.registers.add(usos);

        University agh = new University("AGH");
        agh.registers.add(usos);

        for (University u : Arrays.asList(uj, agh)) {
            // I think here should be some "UniversityManager" that will associate string to University
            // That allows as to have controlled list of all created Universities.
            // Should it then have private (package-protected) constructor?
            System.out.println("University: " + u.name);
            for (University.Register register : uj.registers) {
                System.out.println("Register: " + register.getName());
                for (Person person : register.getStudents()) {
                    System.out.println("Student: (" + (person.isMale() ? "M" : "F") + ") " + person.getName() + " " + person.getSurname() + ", " + person.getPesel());
                }
                System.out.println();
            }
            System.out.println();
        }
    }
    /*
    In general in the project should be packages. Using main package for all classes is non-professional.
    And where on earth are JavaDocs?!
     */
}
