package pl.sdacademy.git.javakrk20;

import java.util.Objects;

/*
Not every person is a Student. Here should be inheritance. This class should be abstract then, I guess.
Additionally pesel should be checked in runtime if it's checksum is correct. If not -> throws an exception.
And I guess toString() method is missing.
And gender should be saved as an enum object, not boolean (nor int etc.) variable.
Is there more genders? :O
 */

abstract public class Person {
    private String name;
    private String surname;
    private String pesel;

    private boolean male;

    public Person(String name, String surname, String pesel, boolean male) {
        this.name = name;
        this.surname = surname;
        this.pesel = pesel;
        this.male = male;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPesel() {
        return pesel;
    }

    public boolean isMale() {
        return male;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public void setMale(boolean male) {
        this.male = male;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Person)) {
            return false;
        }
        Person person = (Person) o;
        return Objects.equals(name, person.name) &&
                Objects.equals(surname, person.surname) &&
                Objects.equals(pesel, person.pesel) &&
                Objects.equals(male, person.male);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, pesel, male);
    }
}
